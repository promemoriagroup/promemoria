<?php
/**
* Template Name: Portfolio
 */

get_header(); ?>

<?php $progetti = new WP_Query(array(
    'posts_per_page' => -1,
    'post_type' => 'progetto',
    'post_status' => 'publish',
    'tax_query'        => array(
	    array(
	        'taxonomy'  => 'tipologia',
	        'terms' => 'speciali',
	        'field' => 'slug',
	        'operator'  => 'NOT IN')
	        ),
     )); ?>
    <?php if ( $progetti->have_posts() ) :  $i=0; ?>
    <div class="row">
			<!-- primo box vuoto -->
			
		<?php while ( $progetti->have_posts() ) : $progetti->the_post(); $img_header = get_field('immagine_cubotto'); if($i%3==0) {  ?>
		
			<?php } ?>
			
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 full-bg item-progetto no-p" style="background-image:url(<?php echo $img_header['url']; ?>)">
					
					<div class="img-overlay whiter"></div>
					
					<div class="cubotti">
						<a href="<?php the_permalink(); ?>" class="inner">
							<h4 class="black text-xs upp nx-b nome-cliente">
				                        <?php 
				                        $terms = get_the_terms( $post->ID, 'cliente' );
				                        $tc = count($terms)-1;
				                        if ( !empty( $terms ) ){
				                            foreach ($terms as $cl=>$term) {
				                                echo ''.$term->name.'';
				                                if($cl < $tc ) echo ' / ';
				                            }
				                        }
				                         ?>
				            </h4>
							<hr class="hr-short-red">
								<h3 class="red text-sm titolo-progetto">
									<?php the_field('titolo_progetto'); ?>
								</h3>
							<div class="pos-left-bottom p-left-2 p-bottom-2 hidden-xs">
								<h4 class="black text-xs upp nx-b">
				                        <?php 
				                        $terms = get_the_terms( $post->ID, 'tipologia' );
				                        $t = count($terms)-1;
				                        if ( !empty( $terms ) ){
				                            foreach ($terms as $c=>$term) {
				                                echo ''.$term->name.'';
				                                if($c < $t ) echo '<br>';
				                            }
				                        }
				                         ?>
				           		 </h4>
							</div>
							<div class="pos-right-bottom p-right-2 p-bottom-2">
								<span class="link-btn"></span>
							</div>
						</a>
					</div>
			</div>
			<?php $i++; if($i==3) {  ?>
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 bg-red no-p hidden-xs hidden-md">
					<div class="cubotti">
						<div class="inner aon">
							<h4 class="white text-xs upp nx-b">
							
							</h4>
							<div class="pos-left-bottom p-left-2 p-bottom-2">
								<h3 class="white text-sm">
									<?php _e('"Vuoi realizzare<br>un progetto con noi?"','promemoria'); ?>
								</h3>
								<h4 class="p-top-2 white">info@promemoriagroup.com</h4>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
			<?php if($i==4) {  ?>
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 bg-red no-p">
					<div class="cubotti">
						<div class="inner aon">
							
								<h3 class="white text-sm p-top-3 p-left pos-cite">"Innovation is more powerful when it springs from a preserved heritage.”<br>
									<small class"pull-right white">- Bernard Arnault</small>
								</h3>
						</div>
					</div>
			</div>
			<?php } ?>
			<?php if($i==6) {  ?>
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 bg-red no-p hidden-xs hidden-md">
					<div class="cubotti">
						<div class="inner aon">
							
								<h3 class="white text-sm p-top-3 p-left pos-cite"><?php _e('"La storia è un grande presente, e mai solamente un passato."','promemoria'); ?>
								<small class"pull-right white">- Alain</small>
								</h3>
						</div>
					</div>
			</div>
			<?php } ?>
			<?php if($i==13) {  ?>
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 bg-red no-p">
					<div class="cubotti">
						<div class="inner aon">
							
								<h3 class="white text-sm p-top-3 p-left pos-cite"><?php _e('"Non è possibile unire i puntini guardando avanti, potete unirli solo girandovi e guardando indietro."','promemoria'); ?><br>
									<small class"pull-right white">- Steve Jobs</small>
								</h3>
						</div>
					</div>
			</div>
			<?php } ?>
			<?php if($i==18) {  ?>
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 bg-red no-p hidden-xs hidden-md">
					<div class="cubotti">
						<div class="inner aon">
							<h4 class="white text-xs upp nx-b">
							
							</h4>
							<div class="pos-left-bottom p-left-2 p-bottom-2">
								<h3 class="white text-sm">
									<?php _e('"Vuoi realizzare<br>un progetto con noi?"','promemoria'); ?>
								</h3>
								<h4 class="p-top-2 white">info@promemoriagroup.com</h4>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 bg-red no-p hidden-xs hidden-md">
					<div class="cubotti">
						<div class="inner aon">
						</div>
					</div>
				</div> -->
			<?php } ?>
		<?php endwhile; ?>
		</div>
	<?php endif; ?>
	<?php wp_reset_query(); ?>

	

<?php get_footer(); ?>


