<?php
/**
* Template Name: Clienti
 */

get_header(); ?>

<?php if( have_rows('top_client') ) : $i=1 ?>

<div class="row">
	
	
	<?php  while(have_rows('top_client') ): the_row(); if($i!=0) :  ?>

		<?php $post_object = get_sub_field('sel_client'); $post = $post_object; setup_postdata( $post ); $chiave = get_field('chiave_cubotto', $post); $anno = get_field('anno', $post); ?>
			<!-- MOBILE -->
			<?php if ($i % 2 == 0): ?>
				<div class="col-xs-6 no-p full-bg item-progetto visible-xs">
					<div class="cubotti">
						<div class="inner always">
							<div class="pos-left-bottom p-left p-bottom-2">
								<p class="text-xs nx"><?php echo $anno; ?></p>
								<h4 class="black text-xs upp nx-b">
				                        <?php  echo $post->name; ?>
				           		 </h4>
				           		 <hr class="hr-long-red always">
							</div>
							<!-- <div class="pos-right-bottom p-right-2 p-bottom-2">
								<span class="link-btn"></span>
							</div> -->
						</div>
					</div>
			</div>
			<div class="col-xs-6 no-p full-bg item-progetto visible-xs" style="background-image:url(<?php echo $chiave['url']; ?>)">
				<div class="cubotti">
					<div class="inner always">
					</div>
				</div>
			</div>
			<?php else : ?>
				<div class="col-xs-6 no-p full-bg item-progetto visible-xs" style="background-image:url(<?php echo $chiave['url']; ?>)">
					<div class="cubotti">
						<div class="inner always">
						</div>
					</div>
				</div>
				<div class="col-xs-6 no-p full-bg item-progetto visible-xs">
					<div class="cubotti">
						<div class="inner always">
							<div class="pos-left-bottom p-left p-bottom-2">
								<p class="text-xs nx"><?php echo $anno; ?></p>
								<h4 class="black text-xs upp nx-b">
				                        <?php  echo $post->name; ?>
				           		 </h4>
				           		 <hr class="hr-long-red always">
							</div>
							<!-- <div class="pos-right-bottom p-right-2 p-bottom-2">
								<span class="link-btn"></span>
							</div> -->
						</div>
					</div>
				</div>
		<?php endif; ?>
			
			<!-- DESK -->
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 no-p full-bg item-progetto hidden-xs" style="background-image:url(<?php echo $chiave['url']; ?>)">
					<div class="cubotti">
						<div class="inner always">
							<div class="pos-left-bottom p-left-2 p-bottom-2">
								<p class="text-xs nx"><?php echo $anno; ?></p>
								<h4 class="black text-xs upp nx-b">
				                        <?php  echo $post->name; ?>
				           		 </h4>
				           		 <hr class="hr-long-red always">
							</div>
							<!-- <div class="pos-right-bottom p-right-2 p-bottom-2">
								<span class="link-btn"></span>
							</div> -->
						</div>
					</div>
			</div>
			<?php wp_reset_postdata(); ?>
	<?php endif; ?>
			
			<?php if($i==3) : ?>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 bg-white no-p hidden-xs">
					<div class="cubotti">
						<div class="inner aon">
							<h3 class="red text-sm p-top-3 p-left pos-cite">
								<?php the_field('frase_1') ?>
							</h3>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<?php if($i==7) :  ?>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 bg-white no-p hidden-xs">
						<div class="cubotti">
							<div class="inner aon">
								
									<h3 class="red text-sm p-top-3 p-left pos-cite">
										<?php the_field('frase_2') ?>
									</h3>
							</div>
						</div>
				</div>
			<?php endif; ?>
			<?php if($i==12) :  ?>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 bg-white no-p hidden-xs">
						<div class="cubotti">
							<div class="inner aon">
								
									<h3 class="red text-sm p-top-3 p-left pos-cite">
										<?php the_field('frase_3') ?>
									</h3>
							</div>
						</div>
				</div>
			<?php endif; ?>
	<?php $i++;  endwhile; ?>
</div>

<?php endif;  ?>

<div class="row p-row-top p-row-bottom">
		<div class="container no-p">
			<div class="col-md-2 ">
				<div class="red-top-3 p-top-2 p-bottom-2 h-com pos-r">
					<h4 class="black text-xs nx-b upp"><?php _e('Lavoriamo con','promemoria'); ?></h4>
				</div>
			</div>
			<div class="col-md-10">
				<div class="red-top-1 p-top-2">
					<div class="two-col black text-xs listato-clienti">
					<?php $terms = get_terms("cliente", array('hide_empty'=> 0)); ?> 
					<?php foreach ( $terms as $term ) : ?> 
						<?php $n_custom = get_field('nome_cliente', $term);  if($n_custom): ?>
							<p class="text-xs"><?php echo $n_custom; ?></p>
						<?php else : ?>
					  		<p class="text-xs"><?php echo $term->name; ?></p>
						<?php endif; ?>
					 <?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>	
	</div>


<?php get_footer(); ?>


