<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Promemoria
 */

get_header(); ?>

		<?php while ( have_posts() ) : the_post(); ?>
			<!-- immagine intro -->
			<div class="header-fade">
				<?php $header_image = get_field('immagine_header'); ?>
					<div class="row <?php if (!empty($header_image)) {echo 'taglio-immagine';} else {echo "taglio-immagine-vuoto";} ;?>" style="background-image:url('<?php echo $header_image['url']; ?>')">
						
					</div>
					<div class="row">
						<div class="container margin-calc">
							<div class="col-md-10 col-md-push-2 bg-white p-top p-left">	
								<?php $tit_color = get_field('colore_titolo') ?>
									<h2 class="red text-md p-left" style="color:<?php echo $tit_color ?>"><?php the_field('titolo_progetto') ?></h2> 
									<div class="p-top-half p-left">
										<?php $sottotitolo = get_field('sottotitolo'); ?>
										<?php if ($sottotitolo): ?>
											<?php $sub_color = get_field('colore_sottotitolo') ?>
											<h3 class="black text-sm sottotitolo" style="color:<?php echo $sub_color ?>"><?php echo $sottotitolo; ?></h3>
										<?php endif ?>
									</div>
							</div>
						</div>
					</div>
			</div>

			<!-- teaser e sottotitolo -->
			<?php $teaser = get_field('teaser'); $desc = get_field('descrizione'); if ($teaser): ?>
			<div class="row p-row-top p-row-bottom">
				<div class="container no-p h-eq">
					<div class="col-md-2 ">
						<div class="red-top-1 red-bottom-1 p-top-2 p-bottom-2 visible-xs pos-r">
							<div class="cat p-bottom">
								<h4 class="nx-b black text-xs upp"><?php _e('CLIENTE','promemoria'); ?></h4>
								<p>
			                        <?php 
			                        $terms = get_the_terms( $post->ID, 'cliente' );
			                        $t = count($terms)-1;
			                        if ( !empty( $terms ) ){
			                            foreach ($terms as $c=>$term) {
			                                echo ''.$term->name.'';
			                                if($c < $t ) echo ' / ';
			                            }
			                        }
			                         ?>
			                    </p>
		                    </div>
		                    <div class="cat p-bottom">
								<h4 class="nx-b black text-xs upp"><?php _e('COMPETENZA','promemoria'); ?></h4>
								<p>
			                        <?php 
			                        $terms = get_the_terms( $post->ID, 'tipologia' );
			                        $t = count($terms)-1;
			                        if ( !empty( $terms ) ){
			                            foreach ($terms as $c=>$term) {
			                                echo ''.$term->name.'';
			                                if($c < $t ) echo ', ';
			                            }
			                        }
			                         ?>
			                    </p>
		                    </div>
		                    <div class="cat p-bottom">
								<h4 class="nx-b black text-xs upp"><?php _e('SETTORE','promemoria'); ?></h4>
								<p>
			                        <?php 
			                        $terms = get_the_terms( $post->ID, 'settore' );
			                        $t = count($terms)-1;
			                        if ( !empty( $terms ) ){
			                            foreach ($terms as $c=>$term) {
			                                echo ''.$term->name.'';
			                                if($c < $t ) echo ', ';
			                            }
			                        }
			                         ?>
			                    </p>
		                    </div>
		                     <div class="cat p-bottom">
								<h4 class="nx-b black text-xs upp"><?php _e('ANNO','promemoria'); ?></h4>
								<p>
			                        <?php 
			                        $terms = get_the_terms( $post->ID, 'anno' );
			                        $t = count($terms)-1;
			                        if ( !empty( $terms ) ){
			                            foreach ($terms as $c=>$term) {
			                                echo ''.$term->name.'';
			                                if($c < $t ) echo ', ';
			                            }
			                        }
			                         ?>
			                    </p>
		                    </div>
		                    
		                    	<?php $link = get_field('sito') ?>
		                    	<?php if ($link): ?>
		                    		<div class="pos-left-bottom p-bottom">
		                    			<a class="link-btn black upp" target="_blank" href="<?php echo $link ?>"><?php _e('Visita il sito','promemoria'); ?></a>
		                   			</div>
		                    	<?php endif ?>
						</div>
						<div class="red-top-1 red-bottom-1 p-top-2 p-bottom-2 h-com hidden-xs pos-r">
							<div class="cat p-bottom">
								<h4 class="nx-b black text-xs upp"><?php _e('CLIENTE','promemoria'); ?></h4>
								<p>
			                        <?php 
			                        $terms = get_the_terms( $post->ID, 'cliente' );
			                        $t = count($terms)-1;
			                        if ( !empty( $terms ) ){
			                            foreach ($terms as $c=>$term) {
			                                echo ''.$term->name.'';
			                                if($c < $t ) echo ' / ';
			                            }
			                        }
			                         ?>
			                    </p>
		                    </div>
		                    <div class="cat p-bottom">
								<h4 class="nx-b black text-xs upp"><?php _e('COMPETENZA','promemoria'); ?></h4>
								<p>
			                        <?php 
			                        $terms = get_the_terms( $post->ID, 'tipologia' );
			                        $t = count($terms)-1;
			                        if ( !empty( $terms ) ){
			                            foreach ($terms as $c=>$term) {
			                                echo ''.$term->name.'';
			                                if($c < $t ) echo ', ';
			                            }
			                        }
			                         ?>
			                    </p>
		                    </div>
		                    <div class="cat p-bottom">
								<h4 class="nx-b black text-xs upp"><?php _e('SETTORE','promemoria'); ?></h4>
								<p>
			                        <?php 
			                        $terms = get_the_terms( $post->ID, 'settore' );
			                        $t = count($terms)-1;
			                        if ( !empty( $terms ) ){
			                            foreach ($terms as $c=>$term) {
			                                echo ''.$term->name.'';
			                                if($c < $t ) echo ', ';
			                            }
			                        }
			                         ?>
			                    </p>
		                    </div>
		                     <div class="cat p-bottom">
								<h4 class="nx-b black text-xs upp"><?php _e('ANNO','promemoria'); ?></h4>
								<p>
			                        <?php 
			                        $terms = get_the_terms( $post->ID, 'anno' );
			                        $t = count($terms)-1;
			                        if ( !empty( $terms ) ){
			                            foreach ($terms as $c=>$term) {
			                                echo ''.$term->name.'';
			                                if($c < $t ) echo ', ';
			                            }
			                        }
			                         ?>
			                    </p>
		                    </div>
		                    
		                    	<?php $link = get_field('sito') ?>
		                    	<?php if ($link): ?>
		                    		<div class="pos-left-bottom p-bottom">
		                    			<a class="link-btn black upp" target="_blank" href="<?php echo $link ?>"><?php _e('Visita il sito','promemoria'); ?></a>
		                   			</div>
		                    	<?php endif ?>
						</div>
					</div>
					<div class="col-md-10 h-com hidden-xs">
						<div class="red-top-1 p-top-2">
							<h3 class="black text-sm">
									<?php echo $teaser; ?>
							</h3>
							<div class="p-top-2">
								<?php echo $desc; ?>
							</div>
						</div>
					</div>
					<div class="col-md-10 visible-xs">
						<div class="red-top-1 p-top-2">
							<h3 class="black text-sm">
									<?php echo $teaser; ?>
							</h3>
							<div class="p-top-2">
								<?php echo $desc; ?>
							</div>
						</div>
					</div>
				</div>	
			</div>
			<?php endif ?>


			<!-- apro flexible box  -->

			<?php while(the_flexible_field("corpo_progetto")): ?>

				<!-- paragrafo -->

				<?php if(get_row_layout() == "paragrafo"): $titolo_paragrafo = get_sub_field('titolo'); $testo_paragrafo = get_sub_field('testo'); ?>

					<div class="row p-top-3 p-bottom-3 ">
						<div class="container no-p">
							<div class="col-md-2">
								
									<?php if ($titolo_paragrafo): ?>
										<h4 class="red text-xs upp nx-b">
											<?php echo $titolo_paragrafo; ?>
										</h4>
									<?php endif ?>
							</div>
							<div class="col-md-10">
								<div class="p-bottom-2">
									<?php echo $testo_paragrafo; ?>
								</div>
							</div>
						</div>	
					</div>

				<!-- immagine -->

				<?php elseif(get_row_layout() == "taglio_immagine"): $taglio_immagine = get_sub_field('immagine'); $size = get_sub_field('image_size'); ?>

					<?php if ($size == 'full'): ?>
						<div class="row taglio-immagine speciale-progetto pos-r" style="background-image:url('<?php echo $taglio_immagine['url']; ?>')">
							<?php $tbox = get_sub_field('text_box'); ?>
							<?php if ($tbox !=""): ?>
								<div class="container text-center">
									<div>
										<h3 class="white text-lg">
											<?php echo $tbox; ?>
										</h3>
									</div>
								</div>
							<?php endif ?>
						</div>
					<?php else : ?>
						<?php $row_bg = get_sub_field('row_bg'); ?>
						<div class="row p-row-top p-row-bottom" style="background-color:<?php echo $row_bg ?>;">
							<div class="container">
								<img class="img-responsive m-auto" src="<?php echo $taglio_immagine['url']; ?>">
							</div>
						</div>
					<?php endif; ?>

				<!-- Immagine libera   -->

				<?php elseif(get_row_layout() == "immagine_full"): $taglio_immagine_full = get_sub_field('immagine'); ?>

					<div class="row">
						<img class="img-responsive m-auto" src="<?php echo $taglio_immagine_full['url']; ?>">
					</div>

				<!-- Video  -->

				<?php elseif(get_row_layout() == "video"):  $yt = get_sub_field('youtube'); $vm = get_sub_field('vimeo'); // layout: Featured Posts ?>

					<div class="row p-row-bottom">
						<div class="container">
							<div id="video">
							<?php if ($yt): ?>
								<?php echo $yt; ?>
							<?php elseif($vm): ?>
								<?php echo $vm; ?>
							<?php endif; ?>
							</div>
						</div>
					</div>
				
				<!-- galleria -->

				<?php elseif(get_row_layout() == "gallery"): // layout: Featured Posts ?>
					<?php $images = get_sub_field('gallery'); if( $images ): ?>
					<div class="row pos-r wrapgal">
					    <div class="gal-progetto">
					        <?php foreach( $images as $image ): ?>
					            <div class="item taglio-immagine" style="background-image:url(<?php echo $image['url']; ?>);">
					                     
					                <!-- <p><?php // echo $image['caption']; ?></p> -->
					            </div>
					        <?php endforeach; ?>
					        
					    </div>
					    <div class="customNavigation gal-buttons lazy">
			                <a class="gal-prev"></a>
			                <a class="gal-next"></a>
			            </div>
					</div>
					<?php endif; ?>

				<!-- Evidenza -->

				<?php elseif(get_row_layout() == "frase_evidenza"):  $evidenza = get_sub_field('evidenza');// layout: Featured Posts ?>
					<div>
						<h3><?php echo $evidenza; ?></h3>
					</div>
				<?php endif; ?>
			<?php endwhile; ?>
		<?php endwhile; // End of the loop. ?>
		<div class="row p-top-3">
					 		<div class="col-md-3 col-md-push-3 no-p"><h4 class="black text-xs upp nx-b titolettoprogetti"><?php _e('Guarda gli altri progetti','promemoria'); ?></h4>
					 		</div>
					 	</div>

		<div class="row m-top">
			<?php $current = $post->ID; $progetti = new WP_Query(array(
		    'posts_per_page' => 4,
		    'post_type' => 'progetto',
		    'post_status' => 'publish',
		    'post__not_in' => array($current)
		     )); ?>
		    <?php if ( $progetti->have_posts() ) :  $i=0; ?>
				<?php while ( $progetti->have_posts() ) : $progetti->the_post(); $img_header = get_field('immagine_header'); ?>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 full-bg item-progetto no-p" style="background-image:url(<?php echo $img_header['url']; ?>)">
					
					<div class="img-overlay whiter"></div>
					
					<div class="cubotti">
						<a href="<?php the_permalink(); ?>" class="inner">
							<h4 class="black text-xs upp nx-b">
				                        <?php $terms = get_the_terms( $post->ID, 'cliente' );
				                        if ( !empty( $terms ) ){
				                            foreach ($terms as $term) {
				                                echo $term->name;
				                            }
				                        } ?>
				            </h4>
							<hr class="hr-short-red">
								<h3 class="red text-sm">
									<?php the_field('titolo_progetto'); ?>
								</h3>
							<div class="pos-left-bottom p-left-2 p-bottom-2">
								<h4 class="black text-xs upp nx-b">
				                        <?php 
				                        $terms = get_the_terms( $post->ID, 'tipologia' );
				                        $t = count($terms)-1;
				                        if ( !empty( $terms ) ){
				                            foreach ($terms as $c=>$term) {
				                                echo ''.$term->name.'';
				                                if($c < $t ) echo '<br>';
				                            }
				                        }
				                         ?>
				           		 </h4>
							</div>
							<div class="pos-right-bottom p-right-2 p-bottom-2">
								<span class="link-btn"></span>
							</div>
						</a>
					</div>
				</div>
				<?php endwhile; ?>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>

		</div>

<?php get_footer(); ?>
