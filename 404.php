<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Promemoria
 */

get_header(); ?>

<div class="header-fade">
	<div class="row taglio-immagine pos-bottom-404" style="background-image:url('http://www.promemoriagroup.com/wp-content/uploads/2016/09/automotive-62827_960_720.jpg')">
	</div>
	<div class="row">
		<div class="container margin-calc">
			<div class="col-md-10 col-md-push-2 bg-white p-top-2 p-bottom p-left">
			<h2 class="red text-md p-left" style="color:<?php echo $tit_color ?>">Oops...Pagina non trovata</h2>
			</div>
		</div>
	</div>
</div>
<div class="row p-top p-bottom">
	<div class="container">
		
	</div>
</div>

<?php get_footer(); ?>
