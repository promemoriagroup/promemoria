<?php
/**
 * 
 * template name: Homepage
 */

get_header('homepage'); ?>

<div class="hidden">
	<?php the_content(); ?> 	
</div>

<div class="row">
	<?php if( have_rows('slider') ): $i = 1; ?>
		<div id="intro-slider" class="container no-p full-h pos-r">
			<?php while ( have_rows('slider') ) : the_row(); $image = get_sub_field('immagine'); $link = get_sub_field('link')?> 
				<?php if ($i == 1): ?>

					<div class="item full-bg" style="background-image:url(<?php echo $image['url'] ?>);">
						<div class="wrapper-chiavi">
							<img src="<?php echo get_bloginfo('template_directory');?>/img/chiave-06.png" class="cv hidden-xs sfasa-t chiave-6">
							<img src="<?php echo get_bloginfo('template_directory');?>/img/chiave-07.png" class="cv hidden-xs sfasa-b chiave-7">
							<img src="<?php echo get_bloginfo('template_directory');?>/img/chiave-08.png" class="cv hidden-xs sfasa-t chiave-8">
							<img src="<?php echo get_bloginfo('template_directory');?>/img/chiave-09.png" class="cv hidden-xs sfasa-b chiave-9">
							<img src="<?php echo get_bloginfo('template_directory');?>/img/chiave-12.png" class="cv hidden-xs sfasa-t chiave-12">
							<img src="<?php echo get_bloginfo('template_directory');?>/img/chiave-14.png" class="cv hidden-xs sfasa-b chiave-14">
							<img src="<?php echo get_bloginfo('template_directory');?>/img/chiave-13.png" class="cv hidden-xs sfasa-t chiave-13">
							<img src="<?php echo get_bloginfo('template_directory');?>/img/chiave-10.png" class="cv hidden-xs sfasa-b chiave-10">
							<img src="<?php echo get_bloginfo('template_directory');?>/img/chiave-15.png" class="cv hidden-xs sfasa-t chiave-15">
							<img src="<?php echo get_bloginfo('template_directory');?>/img/chiave-11.png" class="cv hidden-xs sfasa-b chiave-11">
							<img src="<?php echo get_bloginfo('template_directory');?>/img/chiave-14.png" class="cv sfasa-b chiave-mobile visible-xs">
						</div>
						<div class="frase-intro container no-p hidden-xs">
							<h2 class="red text-lg m-bottom-2">
								<!-- Promemoria /prome’morja/s.m. -->
								
								<?php the_sub_field('titolo') ?>
							</h2>
							<p><?php the_sub_field('sottotitolo') ?></p>
						</div>

						<div class="frase-intro memories col-xs-12 no-p visible-xs bg-white">
							<h2 class="red text-lg m-bottom-2">
								<!-- Promemoria /prome’morja/s.m. -->
								<?php the_sub_field('titolo') ?>
							</h2>
							<p><?php the_sub_field('sottotitolo') ?></p>
						</div>
					</div>
				<?php else : ?>
				<a href="<?php echo $link ?>">
				<div class="item full-bg" style="background-image:url(<?php echo $image['url'] ?>);">
						<div class="img-overlay superw visible-xs"></div>
						<div class="frase-intro container no-p hidden-xs">
							<h2 class="red text-lg m-bottom-2">
								<!-- Promemoria /prome’morja/s.m. -->
								<?php the_sub_field('titolo') ?>
							</h2>
							<p><?php the_sub_field('sottotitolo') ?></p>
						</div>

						<div class="frase-intro col-xs-12 no-p visible-xs bg-white">
							<h2 class="red text-lg m-bottom-2">
								<!-- Promemoria /prome’morja/s.m. -->
								<?php the_sub_field('titolo') ?>
							</h2>
							<p><?php the_sub_field('sottotitolo') ?></p>
						</div>
					
				</div>
				</a>
				<?php endif; ?>
				<?php $i++; ?>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</div>

<!-- CLAIM  -->
<?php $img_claim = get_field('immagine_claim'); ?>
<div class="row taglio-immagine claim pos-r" style="background-image:url(<?php echo $img_claim['url'] ?>);">
	<div class="img-overlay redest"></div>
	<div class="container">
		<div class="col-md-12 no-p">
			<h3 class="white text-md">
				<?php the_field('claim') ?>
			</h3>
		</div>
	</div>
</div>

<!-- FILOSOFIA  -->

<div class="row p-row-top p-row-bottom">
	<div class="container no-p">
		<div class="col-md-2">
			<div class="red-top-3 p-top-2 p-bottom-2">
				<h4 class="black text-xs upp nx-b"><?php the_field('titolo_cs'); ?></h4>
			</div>
		</div>
		<div class="col-md-10">
			<div class="red-top-1 p-top-2 p-bottom-2">
				<h3 class="black text-sm">
						<?php the_field('descrizione_cs'); ?>
				</h3>
				
			</div>
			<div class="read-more upp">
					<?php $link_cs = get_field('link_cs') ?>
					<a class="link-btn" href="<?php echo $link_cs; ?>"><?php _e('Scopri','promemoria'); ?></a>
				</div>
		</div>
	</div>	
</div>

<!-- COSA FACCIAMO Mobile  -->


<?php $childpagesmob = new WP_Query( array(
	'post_type'      => 'page', 
	'post_parent'    => '8',
	'orderby'        => 'menu_order'
	)); ?>
			<div class="row p-top-2 p-bottom-2 bg-grey-light visible-xs">
				<div class="container no-p"><h4 class="black text-xs nx-b upp"><?php _e('Cosa facciamo','promemoria'); ?></h4>
				</div>
			</div>

			<?php if ($childpagesmob->have_posts()) : $i=0; ?>
			<?php while($childpagesmob->have_posts()) : $childpagesmob->the_post(); ?>
			<?php $img_header = get_field('immagine_header') ?>
			<a href="<?php the_permalink(); ?>">
				<div class="row visible-xs full-bg pos-r p-top p-bottom" style="background-image:url(<?php echo $img_header['url']; ?>)">
				<div class="img-overlay redest"></div>
					<div class="p-left p-right pos-r">
						<hr class="hr-short-white">
						<h3 class="white text-sm">
							<?php the_title(); ?>
						</h3>
						<div class="read-more upp p-bottom-2 p-top">
							<span class="link-btn nx-b white"><?php _e('Scopri','promemoria'); ?></span>
						</div>
					</div>
				</div>
			</a>
			<?php endwhile; ?>
			<?php endif; ?>
			<div class="row p-top-3 p-bottom-2 bg-grey-light visible-xs">
				<div class="container no-p">
				</div>
			</div>
<?php wp_reset_query(); ?>


<!-- COSA FACCIAMO  -->


<?php $childpages = new WP_Query( array(
	'post_type'      => 'page', 
	'post_parent'    => '8',
	'orderby'        => 'menu_order'
	)); ?>
			<div class="row p-top-2 p-bottom-2 bg-grey-light hidden-xs">
				<div class="container no-p"><h4 class="black text-xs nx-b upp"><?php _e('Cosa facciamo','promemoria'); ?></h4>
				</div>
			</div>

			<?php if ($childpages->have_posts()) : $i=0; // counter
			while($childpages->have_posts()) : $childpages->the_post();
			if($i%3==0) {  ?>


			<div class="row bg-grey-light hidden-xs">
				<div class="container cont-<?php echo $i ?> white-left-3 white-right-3 white-bottom-3 white-top-3 no-p">
					<?php } ?>
					<?php $img_header = get_field('immagine_cubotto'); ?>
					<div class="col-md-4 cf-item full-bg" style="background-image:url(<?php echo $img_header['url']; ?>)">
						<div class="img-overlay supergrey"></div>
						<a href="<?php the_permalink();?>">
							<div class="inner">
								<hr class="hr-short-red">
								<h3 class="black text-sm">
										<?php the_title(); ?>
								</h3>
								<div class="pos-left-bottom read-more upp p-left-2 p-bottom-2">
									<span class="link-btn nx-b"><?php _e('Scopri','promemoria'); ?></span>
								</div>
							</div>
						</a>
					</div>
					<?php $i++;
					if($i%3==0) {  ?>
				</div>
			</div>
			<?php } ?>

		<?php endwhile; ?>
		<?php if($i%3!=0) {  ?>
			</div>
		</div>

<?php } ?>
<?php endif; ?>
			<div class="row p-top-3 p-bottom-2 bg-grey-light hidden-xs">
				<div class="container no-p">
				</div>
			</div>
<?php wp_reset_query(); ?>

<!-- Progetti esempio -->
<?php if( have_rows('progetti_hp') ):  ?>
	<div class="row p-row-top">
		<div class="col-md-9 col-md-push-3 no-p bg-pink"><h4 class="red text-xs nx-b upp p-tot"> <?php _e('Alcuni nostri progetti','promemoria'); ?></h4>
		</div>
	</div>
	<div class="row">
		<?php 
		$row = get_field('progetti_hp'); 
		$total_row = count($row);
		$current_row = 1;
		?>
			<?php while(have_rows('progetti_hp') ): the_row();  ?>
				
				<?php $post_object = get_sub_field('progetto'); if( $post_object ): $post = $post_object; setup_postdata( $post );  $img_header = get_field('immagine_header'); ?>
				<div class="<?php if ($total_row % 4 == 0) {echo 'col-lg-3 col-md-3 col-sm-6';} elseif ($total_row % 3 == 0) {echo 'col-lg-4 col-md-4 col-sm-6';} else {echo 'col-lg-6 col-md-6 col-sm-12';}; ?> col-xs-12 full-bg item-progetto no-p" style="background-image:url(<?php echo $img_header['url']; ?>)">

					<div class="img-overlay whiter"></div>

					<div class="<?php if ($total_row % 4 == 0) {echo 'cubotti';} elseif ($total_row % 3 == 0) {echo 'cubotti';} else {echo 'half-cubotti';}; ?>">
						<a href="<?php the_permalink(); ?>" class="inner">
							<h4 class="black text-xs upp nx-b">
								<?php $terms = get_the_terms( $post->ID, 'cliente' );
								if ( !empty( $terms ) ){
									foreach ($terms as $term) {
										echo $term->name;
									}
								} ?>
							</h4>
							<hr class="hr-short-red">
							<h3 class="red text-sm">
								<?php the_field('titolo_progetto'); ?>
							</h3>
							<div class="pos-left-bottom p-left-2 p-bottom-2">
								<h4 class="black text-xs upp nx-b">
									<?php 
									$terms = get_the_terms( $post->ID, 'tipologia' );
									$t = count($terms)-1;
									if ( !empty( $terms ) ){
										foreach ($terms as $c=>$term) {
											echo ''.$term->name.'';
											if($c < $t ) echo ', ';
										}
									}
									?>
								</h4>
							</div>
							<div class="pos-right-bottom p-right-2 p-bottom-2">
								<span class="link-btn"></span>
							</div>
						</a>
					</div>
				</div>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>
		<?php endwhile;?>
	</div>
<?php endif; ?>


<!-- Progetto singolo  -->

<?php $ps_sfondo = get_field('immagine_sfondo');  ?>
<?php $ps_link = get_field('link_ps'); ?>
<a target="_blank" href="<?php echo $ps_link ?>">
<div class="row taglio-immagine pos-r" style="background-image:url('<?php echo $ps_sfondo['url']; ?>')">
	<div class="img-overlay whiter"></div>
	<div class="container text-center">
		<div>
			<h3 class="red text-lg p-bottom">
				<?php the_field('titolo_ps'); ?>
			</h3>
			<h4 class="black text-sm"><?php the_field('descrizione_ps'); ?></h4>
			
			<div class="p-top-3">	
				<span class="black link-btn upp nx-b"><?php _e('Scopri','promemoria'); ?></span>
			</div>
		</div>
	</div>
</div>
</a>

<!-- NEWS -->

<?php if( have_rows('news_hp')):  ?>

    	<div class="row p-top-2 p-bottom-2 bg-grey-light">
			<div class="container no-p"><h4 class="black text-xs nx-b upp">News</h4></div>
		</div>
		<div class="row bg-grey-light">
			<div class="container white-left-3 white-right-3 white-top-3 no-p">	
				<?php while(have_rows('news_hp') ): the_row();  ?>
					<?php $post_object = get_sub_field('news'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
							<?php $isshort = get_field('is_short') ?>
							<?php $link = get_permalink(); ?>
							<?php if($isshort == 'si') : ?>
							<span class="shortn wrap">
								<div class="row no-m white-bottom-3 h-eq news-item">
									<div class="col-md-2 h-com bg-grey-light h-com hidden-xs">
										<div class="data-holder text-center hidden-xs">
											<div class="black text-xs upp nx-b"><?php the_time('j M'); ?></div>
											<div class="black text-xs upp nx-b anno"><?php the_time('Y'); ?></div>
										</div>	
									</div>
									<div class="col-md-8 h-com  p-top-2 p-bottom-2 news-short h-com hidden-xs bg-grey-light">
										<div class="p-top p-left p-right p-bottom">
											<p class="red text-xs upp visible-xs"><?php the_time('j M Y'); ?></p>
											<h3 class="black text-xs m-bottom-half">
												<?php the_title(); ?>
											</h3>
											<?php the_field('testo_short'); ?>
											<div class="visible-xs">
												<span class="black link-btn nx-b hv-centered w-100 text-center upp"><?php _e('Scopri','promemoria'); ?></span>
											</div>
										</div>
									</div>
									<div class="col-md-8 h-com  p-top-2 p-bottom-2 news-short h-com visible-xs bg-grey-light">
										<div class="p-top p-left p-right p-bottom">
											<p class="red text-xs upp visible-xs"><?php the_time('j M Y'); ?></p>
											<h3 class="black text-xs m-bottom-half">
												<?php the_title(); ?>
											</h3>
											<?php the_field('testo_short'); ?>
											<div class="visible-xs">
												<?php if ($isshort == "no"): ?>
													<span class="black link-btn nx-b hv-centered w-100 text-center upp"><?php _e('Scopri','promemoria'); ?></span>
												<?php endif ?>
											</div>
										</div>
									</div>
									<div class="col-md-2 h-com bg-grey-light h-com hidden-xs">
										
										<?php if ($isshort == "no"): ?>
											<span class="black link-btn nx-b hv-centered w-100 text-center upp"><?php _e('Scopri','promemoria'); ?></span>
										<?php endif ?>
									</div>	
								</div>
							</span>
							<?php elseif ($isshort == 'no') : ?>
							<a class="wrap" href="<?php echo $link; ?>">
								<div class="row no-m white-bottom-3 h-eq news-item">
									<div class="col-md-2 h-com bg-grey-light h-com hidden-xs">
										<div class="data-holder text-center hidden-xs">
											<div class="black text-xs upp nx-b"><?php the_time('j M'); ?></div>
											<div class="black text-xs upp nx-b anno"><?php the_time('Y'); ?></div>
										</div>	
									</div>
									<div class="col-md-8 h-com  p-top-2 p-bottom-2 news-short h-com hidden-xs bg-grey-light">
										<div class="p-top p-left p-right p-bottom">
											<p class="red text-xs upp visible-xs"><?php the_time('j M Y'); ?></p>
											<h3 class="black text-xs m-bottom-half">
												<?php the_title(); ?>
											</h3>
											<?php the_field('testo_short'); ?>
											<div class="visible-xs">
												<span class="black link-btn nx-b hv-centered w-100 text-center upp"><?php _e('Scopri','promemoria'); ?></span>
											</div>
										</div>
									</div>
									<div class="col-md-8 h-com  p-top-2 p-bottom-2 news-short h-com visible-xs bg-grey-light">
										<div class="p-top p-left p-right p-bottom">
											<p class="red text-xs upp visible-xs"><?php the_time('j M Y'); ?></p>
											<h3 class="black text-xs m-bottom-half">
												<?php the_title(); ?>
											</h3>
											<?php the_field('testo_short'); ?>
											<div class="visible-xs">
												<?php if ($isshort == "no"): ?>
													<span class="black link-btn nx-b hv-centered w-100 text-center upp"><?php _e('Scopri','promemoria'); ?></span>
												<?php endif ?>
											</div>
										</div>
									</div>
									<div class="col-md-2 h-com bg-grey-light h-com hidden-xs">
										
										<?php if ($isshort == "no"): ?>
											<span class="black link-btn nx-b hv-centered w-100 text-center upp"><?php _e('Scopri','promemoria'); ?></span>
										<?php endif ?>
									</div>	
								</div>
							</a>
							<?php endif; ?>
							
							
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
				<?php endwhile; ?>
			</div>
		</div>
		<div class="row p-top-3 p-bottom-2 bg-grey-light row-special-m-bottom">
			<div class="container no-p">
			</div>
		</div>
 	<?php endif; ?>



<?php get_footer(); ?>
