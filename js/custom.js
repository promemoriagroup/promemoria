(function($){

	$('.shortn').removeAttr("href");

	//preload
	$(window).load(function() { // makes sure the whole site is loaded
			
            $('#status').fadeOut(); // will first fade out the loading animation

            $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
            $('body').delay(350).css({'overflow':'visible'});

            homeAnimationInit();
            $('.cv').removeClass('sfasa-t');
            $('.cv').removeClass('sfasa-b');
        });
	
	// accrocchio height detection
	function setHeight() {
		windowHeight = $(window).innerHeight();
		$('.full-h').css('min-height', windowHeight);
		$('#intro-slider .item').css('min-height', windowHeight);
	};

	// margin auto calc 
	function margincalc() {
		$('.margin-calc').css('margin-top', -($('.margin-calc').innerHeight()));
	}; 

	setHeight();
	margincalc();

	$(window).resize(function(){
	margincalc();
    setHeight();
	});


	// chiavi 
	function homeAnimationInit() {
	    function e(e, r, o, s, a, u, l, c) {
	        t = u * a + r * (1 - a),
	        n = l * a + o * (1 - a),
	        // i = c * a + s * (1 - a),
	        e.css({
	            // transform: "rotate(" + i + "deg)",
	            left: n + "%"
	        })
	    }
	    var t = 0
	      , n = 0;
	      // , i = 0;
	    $.ScrollCraft({
	        scrollRange: [0, .50],
	        onUpdate: function(t) {
	            e($(".chiave-9"), 50, 50, 10, t, 10, 115, 50),
	            e($(".chiave-6"), 30, 65, 0, t, 0, 90, 30),
	            e($(".chiave-8"), 40, 75, 0, t, 0, -10, 40),
	            e($(".chiave-7"), 40, 60, 0, t, 0, 80, 40),
	            e($(".chiave-14"), 55, 55, 0, t, 0, 115, 55),
	            e($(".chiave-mobile"), 25, 25, 0, t, 0, 115, 25),

	            e($(".chiave-12"), 20, 90, 0, t, 0, 115, 20),
	            e($(".chiave-13"), 50, 80, 0, t, 0, 115, 50),
	            e($(".chiave-10"), 0, 65, 0, t, 0, 0, 0),
	            e($(".chiave-15"), 0, 80, 0, t, 0, 0, 0),
	            e($(".chiave-11"), 0, 70, 0, t, 0, 0, 0)
	        }
	    });
	}
	
	
		
	// accrocchio menu
	var $header = $('#masthead');
	var $main = $('.main-navigation');
	var $nav = $('.navbar-nav');
	var $logob = $('.logo-large .scritta');
	var $logobr = $('.logo-large .round');
	var hoscrollato = false;
	var $item = $('.menu-item');
	function showoff() {
		if ($(window).load) {
			$header.on("mouseover", function() {

				hide_logo();
				show_header();
			});
			$header.mouseout( function() {
				if (!hoscrollato) {

					show_logo();
					hide_header();
				}
			});
		}
	};
	showoff();
	function show_header() {
		$nav.addClass("nav--visible");
		$main.css('background', '#ffffff');
	}
	function hide_header() {
		$nav.removeClass("nav--visible");
		$main.css('background', 'transparent');
	}
	function hide_logo() {
		$logob.addClass("top");
		$logobr.css('left', '100' + 'px');
	}
	function show_logo() {
		$logob.removeClass("top");
		$logobr.css('left', '100' + 'px');
	}
	function current() {
		$item.on('click', function () {
			$(this).addClass('current-menu-item');
		});
		
	}
	current();
	
	// show logo
	$(window).scroll(function(){
		if ($(window).scrollTop() > 0) {
			hoscrollato = true;
			show_header();
			hide_logo();
			$('.dropdown-toggle').attr("aria-expanded","false");
			$('li.dropdown').removeClass('open');
			$('li.dropdown').removeClass('current-menu-item');
		} else if ($(window).scrollTop() == 0) {
			hoscrollato = false;
			hide_header();
			show_logo();
			$('li.dropdown').removeClass('open current-menu-item');
		};
	});

	// video 

  	$("#video").fitVids();
	
	// slider in homepage
	$("#intro-slider").owlCarousel({
		navigation : false, 
		slideSpeed : 300,
		paginationSpeed : 1000,
		autoPlay: 10000,
		loop: true,
		dots: true,
		navigationText: ['<','>'],
		singleItem:true,
		transitionStyle: 'fade'
	});

	// Slider Progetto + navigazione
	var galprogetto = $(".wrapgal");
	galprogetto.each(function(){
		var car = $(this).find('.gal-progetto'),
		navigation = $(this).find('.customNavigation'),
		nextBtn = navigation.find('.gal-next'),
        prevBtn = navigation.find('.gal-prev');
		car.owlCarousel({
			navigation : false, 
			pagination: false,
			slideSpeed : 800,
			paginationSpeed : 400,
			singleItem : true,
			afterAction: function(){
					if ( this.itemsAmount > this.visibleItems.length ) {
						$('.gal-next').show();
						$('.gal-prev').show();

						$('.gal-next').removeClass('disabled');
						$('.gal-prev').removeClass('disabled');
						if ( this.currentItem == 0 ) {
							$('.gal-prev').addClass('disabled');
						}
						if ( this.currentItem == this.maximumItem ) {
							$('.gal-next').addClass('disabled');
						}
					} else {
						$('.gal-next').hide();
						$('.gal-prev').hide();
					}
			}
		});
		nextBtn.click(function(){
			car.trigger('owl.next');    
		});
		prevBtn.click(function(){
			car.trigger('owl.prev');
		});
	});
	
		


	// same height 

	

	if(/(android|bb\d+|meego).+mobile|iphone|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ) {
	 	$.each($('.h-eq'), function() {
			var height = $(this).outerHeight();
			$(this).find('.h-com').css({'height':'auto', 'min-height':'auto'})
		});
	}

	$.each($('.h-eq'), function() {
		var height = $(this).outerHeight();
		$(this).find('.h-com').css('min-height', height)
	});


  	// accrocchio header opacity

  	var header = $('.header-fade');
  	var range = 400;
  	if(header.length >0 ){
  		$(window).on('scroll', function () {
  			var scrollTop = $(this).scrollTop();
  			var offset = header.offset().top;
  			var height = header.outerHeight();
  			offset = offset + height / 1.2;
  			var calc = 1 - (scrollTop - offset + range) / range;
  			header.css({ 'opacity': calc });
  			if ( scrollTop == '0' ) {
  				header.css({ 'opacity': 1 });
  			} else if ( calc < '0' ) {
  				header.css({ 'opacity': 0 });
  			}
  		});
  	}	



  })(jQuery);



