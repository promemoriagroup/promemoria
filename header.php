<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_bloginfo('template_directory');?>/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_bloginfo('template_directory');?>/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_bloginfo('template_directory');?>/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_bloginfo('template_directory');?>/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_bloginfo('template_directory');?>/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_bloginfo('template_directory');?>/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_bloginfo('template_directory');?>/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_bloginfo('template_directory');?>/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_bloginfo('template_directory');?>/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_bloginfo('template_directory');?>/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_bloginfo('template_directory');?>/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_bloginfo('template_directory');?>/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_bloginfo('template_directory');?>/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_bloginfo('template_directory');?>/img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo get_bloginfo('template_directory');?>/img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-60799573-1', 'auto');
  ga('send', 'pageview');
</script>
</head>
<body <?php body_class(); ?>>
<!-- Preloader -->
<div id="page" class="site container-fluid">	
	<header id="masthead" class="site-header inside" role="banner">
		<nav id="site-navigation" class="main-navigation bg-white navbar navbar-default navbar-fixed-top" role="navigation">
			<div class="nav-wrapper container">
				<!-- logo intro completo -->	
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<div class="logo-large">
						<img class="round" src="<?php echo get_bloginfo('template_directory');?>/img/logo_footer.png">
					</div>
				</a>
				<!-- logo sulla barra -->
				<!-- <div class="logo-short">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_bloginfo('template_directory');?>/img/logo_short.png"></a>
				</div> -->
				<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', '_s' ); ?></a>
	            <div class="navbar-header">
	                <button type="button" class="navbar-toggle pull-right m-right-2" data-toggle="collapse" data-target="#navbar-collapse-main">
	                    <span class="sr-only"><?php _e('Toggle navigation', '_s'); ?></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
	                <!--<a class="navbar-brand" href="#">Brand</a>-->
	            </div>
	            <div class="collapse navbar-collapse" id="navbar-collapse-main">
		            <ul class="nav navbar-nav pull-right nav--visible always--visible">
			            <?php if( has_nav_menu( 'primary' ) ) :
				            wp_nav_menu( array(
			                        'theme_location'  => 'primary',
			                        'container'       => false,
			                        //'menu_class'      => 'nav navbar-nav',//  'nav navbar-right'
			                        'walker'          => new Bootstrap_Nav_Menu(),
			                        'fallback_cb'     => null,
					                'items_wrap'      => '%3$s',// skip the containing <ul>
			                    )
			                );
		                else :
			                wp_list_pages( array(
					                'menu_class'      => 'nav navbar-nav',//  'nav navbar-right'
					                'walker'          => new Bootstrap_Page_Menu(),
					                'title_li'        => null,
				                )
			                );
			            endif; ?>
		            </ul>
		            <?php // get_search_form(); ?>
	            </div><!-- /.navbar-collapse -->
	        </div>
		</nav><!-- #site-navigation -->
		<div class="site-branding hidden">
			<?php if ( is_front_page() || is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->
	</header><!-- #masthead -->
<?php $detect = new Mobile_Detect; ?>