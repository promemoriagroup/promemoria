<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Promemoria
 */

get_header(); ?>

		<?php while ( have_posts() ) : the_post(); ?>
			<!-- immagine intro -->
			<div class="header-fade">
				<?php $header_image = get_field('immagine_header'); ?>
					<div class="row <?php if (!empty($header_image)) {echo 'taglio-immagine';} else {echo "taglio-immagine-vuoto";} ;?>" style="background-image:url('<?php echo $header_image['url']; ?>')">
						
					</div>
					<div class="row">
						<div class="container margin-calc">
							<div class="col-md-10 col-md-push-2 bg-white p-top-2 p-bottom p-left">	
								<?php $tit_color = get_field('colore_titolo') ?>
									<h2 class="red text-md p-left" style="color:<?php echo $tit_color ?>"><?php the_title(); ?></h2> 
									<div class="p-top-half p-left">
										<?php $sottotitolo = get_field('sottotitolo'); ?>
										<?php if ($sottotitolo): ?>
											<?php $sub_color = get_field('colore_sottotitolo') ?>
											<h3 class="black text-sm sottotitolo" style="color:<?php echo $sub_color ?>"><?php echo $sottotitolo; ?></h3>
										<?php endif ?>
									</div>
							</div>
						</div>
					</div>
			</div>

			<!-- teaser e sottotitolo -->
			<?php $teaser = get_field('teaser'); if ($teaser): ?>
			<div class="row p-top-3 p-bottom-3">
				<div class="container">
					<div class="col-md-2">
						<div class="p-top-2 p-bottom-2">
						</div>
					</div>
					<div class="col-md-10">
						<div class="p-top-2 p-bottom-2">
							<h3 class="red text-sm">
									<?php echo $teaser; ?>
							</h3>
						</div>
					</div>
				</div>	
			</div>
			<?php else : ?> 
				<div class="row p-top p-bottom">
				</div>
			<?php endif; ?>


			<!-- apro flexible box  -->

			<?php while(the_flexible_field("corpo_pagina")): ?>

				<!-- paragrafo -->

				<?php if(get_row_layout() == "paragrafo"): $titolo_paragrafo = get_sub_field('titolo'); $testo_paragrafo = get_sub_field('testo'); $testo_grande_assai = get_sub_field('testo_grande'); ?>

					<div class="row p-top p-bottom-3">
						<div class="container no-p h-eq">
							<div class="col-md-2 visible-xs">
								<?php if ($titolo_paragrafo): ?>
								<div class="red-top-3 p-top-2 p-bottom-2">
									
										<h4 class="red text-xs upp nx-b">
											<?php echo $titolo_paragrafo; ?>
										</h4>
									
								</div>
								<?php endif ?>
								<?php $link = get_sub_field('sito') ?>
		                    	<?php if ($link): ?>
		                    		<div class="pos-left-bottom no-left p-bottom-2">
		                    			<a class="link-btn black upp" target="_blank" href="<?php echo $link ?>"><?php _e('Visita il sito','promemoria'); ?></a>
		                   			</div>
		                    	<?php endif ?>
							</div>
							<div class="col-md-2 h-com hidden-xs">
								<?php if ($titolo_paragrafo): ?>
								<div class="red-top-3 p-top-2 p-bottom-2">
									
										<h4 class="red text-xs upp nx-b">
											<?php echo $titolo_paragrafo; ?>
										</h4>
									
								</div>
								<?php endif ?>
								<?php $link = get_sub_field('sito') ?>
		                    	<?php if ($link): ?>
		                    		<div class="pos-left-bottom no-left p-bottom-2">
		                    			<a class="link-btn black upp" target="_blank" href="<?php echo $link ?>"><?php _e('Visita il sito','promemoria'); ?></a>
		                   			</div>
		                    	<?php endif ?>
							</div>
							<div class="col-md-10">
								<div class="red-top-1 p-top-2 p-bottom-2 testo-paragrafo">
								<?php if ($testo_grande_assai): ?>
									<h3 class="black text-sm p-bottom-2"><?php echo $testo_grande_assai; ?></h3>
								<?php endif ?>
									<?php echo $testo_paragrafo; ?>
								</div>
							</div>
						</div>	
					</div>

				<!-- immagine -->

				<?php elseif(get_row_layout() == "taglio_immagine"): $taglio_immagine = get_sub_field('immagine'); $size = get_sub_field('image_size'); ?>

					<?php if ($size == 'full'): ?>
						<div class="row taglio-immagine pos-r m-bottom-2" style="background-image:url('<?php echo $taglio_immagine['url']; ?>')">
							<?php $tbox = get_sub_field('text_box'); ?>
							<?php if ($tbox !=""): ?>
								<div class="img-overlay redest"></div>
								<div class="container text-center">
									<div>
										<h3 class="white text-lg">
											<?php echo $tbox; ?>
										</h3>
									</div>
								</div>
							<?php endif ?>
						</div>
					<?php else : ?>
						<div class="row p-row-top p-bottom-3 m-bottom-2">
							<div class="container no-p">
								<div class="col-md-2">
									<div class="red-top-3 p-top-2 p-bottom-2">
										
									</div>
								</div>
								<div class="col-md-10">
									<div class="red-top-1 p-top-2 p-bottom-2">
										<img class="img-responsive" src="<?php echo $taglio_immagine['url']; ?>">
									</div>
								</div>
							</div>	
						</div>
					<?php endif; ?>

				<!-- galleria -->

				<?php elseif(get_row_layout() == "gallery"): // layout: Featured Posts ?>
					<?php $images = get_sub_field('gallery'); if( $images ): ?>
					<div class="row">
					    <ul>
					        <?php foreach( $images as $image ): ?>
					            <li class="col-lg-5">
					                     <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					                <!-- <p><?php // echo $image['caption']; ?></p> -->
					            </li>
					        <?php endforeach; ?>
					    </ul>
					</div>
					<?php endif; ?>


			 	<!-- TBox testo/numero -->
				<?php elseif(get_row_layout() == "testi_numeri"): ?>
				 	<?php if( have_rows('tn_box') ): ?>
				 		<div class="row p-row-bottom">
							<div class="container no-p">
								<div class="col-md-2">
									<div class="p-top-2 p-bottom-2">
										
									</div>
								</div>
								<div class="col-md-10 no-p">
									<?php while( have_rows('tn_box') ): the_row(); ?>
										<div class="col-md-4 col-sm-6 col-xs-12 p-bottom-3">		
										<h3 class="black text-md"><?php the_sub_field('numero') ?></h3>
										<hr class="hr-short-red">
										<h3 class="red text-sm"><?php the_sub_field('testo') ?></h3>
										</div>		
									<?php endwhile; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>

				<!-- Progetti esempio -->
				<?php elseif(get_row_layout() == "progetti_esempio"): ?>
				 	<?php if( have_rows('inserisci_progetti') ): ?>
				 		<div class="row p-bottom">
					 		<div class="col-md-3 col-md-push-3 no-p"><h4 class="black text-xs upp nx-b">Alcuni nostri progetti</h4>
					 		</div>
					 	</div>
				 		<div class="row">
				 			<?php 
							$row = get_field('inserisci_progetti'); 
							$total_row = count($row);
							$current_row = 1;
							?>
							<?php while( have_rows('inserisci_progetti') ): the_row(); ?>
										
										<?php $post_object = get_sub_field('progetto'); if( $post_object ): $post = $post_object; setup_postdata( $post );  $img_header = get_field('immagine_cubotto'); ?>
										    	<div class="<?php if ($total_row % 4 == 0) {echo 'col-lg-3 col-md-3 col-sm-6';} elseif ($total_row % 3 == 0) {echo 'col-lg-4 col-md-4 col-sm-6';} elseif ($total_row % 2 == 0) {echo 'col-lg-6 col-md-6 col-sm-6 due-cubotti';} else {echo 'col-lg-6 col-md-6 col-sm-12 due-cubotti';}; ?> col-xs-12 full-bg item-progetto no-p" style="background-image:url(<?php echo $img_header['url']; ?>)">
										    		
													<div class="img-overlay whiter"></div>
													
													<div class="cubotti">
														<a href="<?php the_permalink(); ?>" class="inner">
															<h4 class="black text-xs upp nx-b">
												                        <?php $terms = get_the_terms( $post->ID, 'cliente' );
												                        if ( !empty( $terms ) ){
												                            foreach ($terms as $term) {
												                                echo $term->name;
												                            }
												                        } ?>
												            </h4>
															<hr class="hr-short-red">
																<h3 class="red text-sm">
																	<?php the_field('titolo_progetto'); ?>
																</h3>
															<div class="pos-left-bottom p-left-2 p-bottom-2">
																<h4 class="black text-xs upp nx-b">
												                        <?php 
												                        $terms = get_the_terms( $post->ID, 'tipologia' );
												                        $t = count($terms)-1;
												                        if ( !empty( $terms ) ){
												                            foreach ($terms as $c=>$term) {
												                                echo ''.$term->name.'';
												                                if($c < $t ) echo '<br>';
												                            }
												                        }
												                         ?>
												           		 </h4>
															</div>
															<div class="pos-right-bottom p-right-2 p-bottom-2">
																<span class="link-btn"></span>
															</div>
														</a>
													</div>
												</div>
										    <?php wp_reset_postdata(); ?>
										<?php endif; ?>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>

				<!-- Evidenza -->

				<?php elseif(get_row_layout() == "frase_evidenza"):  $evidenza = get_sub_field('evidenza');// layout: Featured Posts ?>

				<div class="row">
					<div class="container no-p">
						<div class="col-md-2">
							<div class="p-top-2 p-bottom-2">
							</div>
						</div>
						<div class="col-md-10">
							<p><?php echo $evidenza; ?></p>
						</div>
	                </div>
				</div>

				<?php endif; ?>

			<?php endwhile; ?>

			<?php $fp_title =	get_field('fp_titolo'); ?>
				<?php if ($fp_title): ?>
					<div class="row bg-pink p-top p-bottom-3 fix-250">
						<div class="container">
							<div class="col-md-2">
								<div class="red-top-3 p-top-2 p-bottom-2 h-com pos-r">
									<h4 class="red text-xs nx-b upp"><?php the_field('fp_titoletto') ?></h4>
								</div>
							</div>
							<div class="col-md-10">
								<div class="red-top-1 p-top-2 p-bottom-2">
								<h3 class="red text-lg pull-left">
									<?php echo $fp_title ?>
								</h3>
								<div class="linkesterno pull-right p-top">
									<?php $fp_link = get_field('fp_link'); ?> 	
									<a class="black link-btn upp nx-b" href="<?php echo $fp_link; ?>" target='_black'><?php the_field('fp_testopulsante'); ?></a>
								</div>
								</div>
							</div>
						</div>
					</div>
					
				<?php endif ?>
		<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
