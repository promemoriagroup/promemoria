<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Promemoria
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>



	<!-- immagine intro -->
	<div class="header-fade">
		<?php $header_image = get_field('immagine_header'); ?>
		<div class="row <?php if (!empty($header_image)) {echo 'taglio-immagine';} else {echo "taglio-immagine-vuoto";} ;?>" style="background-image:url('<?php echo $header_image['url']; ?>')">

		</div>
		<div class="row">
			<div class="container margin-calc">
				<div class="col-md-10 col-md-push-2 bg-white p-top-2 p-bottom p-left">	
					<?php $tit_color = get_field('colore_titolo') ?>
					<h2 class="red text-md p-left" style="color:<?php echo $tit_color ?>"><?php the_title(); ?></h2> 
					<div class="p-top-half p-left">
						<?php $sottotitolo = get_field('sottotitolo'); ?>
						<?php if ($sottotitolo): ?>
							<?php $sub_color = get_field('colore_sottotitolo') ?>
							<h3 class="black text-sm sottotitolo" style="color:<?php echo $sub_color ?>"><?php echo $sottotitolo; ?></h3>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- apro flexible box  -->

	<?php while(the_flexible_field("corpo")): ?>
			<!-- paragrafo -->
			<?php if(get_row_layout() == "paragrafo"): $titolo_paragrafo = get_sub_field('titolo_paragrafo'); $testo_paragrafo = get_sub_field('testo_paragrafo'); $testo_grande_assai = get_sub_field('testo_grande'); ?>
				<div class="row p-top-3 p-bottom">
					<div class="container no-p">
						<div class="col-md-2">
							<?php if ($titolo_paragrafo): ?>
								<div class="red-top-3 p-top-2 p-bottom-2">

									<h4 class="red text-xs upp nx-b">
										<?php echo $titolo_paragrafo; ?>
									</h4>

								</div>
							<?php endif ?>
						</div>
						<div class="col-md-10">
							<div class="red-top-1 p-top-2 testo-paragrafo">
								<?php if ($testo_grande_assai): ?>
									<h3 class="black text-sm p-bottom-2"><?php echo $testo_grande_assai; ?></h3>
								<?php endif ?>
								<?php echo $testo_paragrafo; ?>
							</div>
						</div>
					</div>	
				</div>

				<!-- immagine -->

			<?php elseif(get_row_layout() == "immagine_standard"): $taglio_immagine = get_sub_field('immagine'); $size = get_sub_field('image_size'); ?>

				<?php if ($size == 'full'): ?>
					<div class="row taglio-immagine pos-r m-row-bottom" style="background-image:url('<?php echo $taglio_immagine['url']; ?>')">

					</div>
				<?php else : ?>
					<div class="row p-bottom-3 ">
						<div class="container no-p">
							<div class="col-md-2">
								<div class="p-top-2 p-bottom-2">

								</div>
							</div>
							<div class="col-md-10">
								<div class="p-bottom-2">
									<img class="img-responsive" src="<?php echo $taglio_immagine['url']; ?>">
								</div>
								<?php $dida = get_sub_field('dida'); ?>
								<?php if ($dida): ?>
									<div class="text-center">
										<hr class="hr-red-centered-short m-bottom">
										<?php echo $dida; ?>
									</div>
								<?php endif ?>
							</div>
						</div>	
					</div>
				<?php endif; ?>

				<!-- Video  -->

			<?php elseif(get_row_layout() == "video"):  $code = get_sub_field('embed'); // layout: Featured Posts ?>

				<div class="row p-row-bottom">
					<div class="container">
						<div id="video">
							<?php echo $code; ?>
						</div>
					</div>
				</div>

			<!-- Evidenza -->

				<?php elseif(get_row_layout() == "evidenza"):  $evidenza = get_sub_field('testo');// layout: Featured Posts ?>

				<div class="row p-top-3 p-bottom">
					<div class="container no-p">
						<div class="col-md-2">
							<div class="p-top-2 p-bottom-2">
							</div>
						</div>
						<div class="col-md-10">
							<p><?php echo $evidenza; ?></p>
						</div>
	                </div>
				</div>


				<!-- galleria -->

			<?php elseif(get_row_layout() == "gallery"): // layout: Featured Posts ?>
				<?php $images = get_sub_field('gallery'); if( $images ): ?>
				<div class="row">
					<ul>
						<?php foreach( $images as $image ): ?>
							<li class="col-lg-5">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								<!-- <p><?php // echo $image['caption']; ?></p> -->
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>


			<!-- TBox testo/numero -->
		<?php elseif(get_row_layout() == "testi_numeri"): ?>
			<?php if( have_rows('tn_box') ): ?>
				<div class="row p-row-bottom">
					<div class="container no-p">
						<div class="col-md-2">
							<div class="p-top-2 p-bottom-2">

							</div>
						</div>
						<div class="col-md-10 no-p">
							<?php while( have_rows('tn_box') ): the_row(); ?>
								<div class="col-md-4 p-bottom-2	">		
									<h3 class="black text-md"><?php the_sub_field('numero') ?></h3>
									<hr class="hr-short-red">
									<h3 class="red text-sm"><?php the_sub_field('testo') ?></h3>
								</div>		
							<?php endwhile; ?>
						</div>
					</div>
				</div>
			<?php endif; ?>

			<!-- Progetti esempio -->
		<?php elseif(get_row_layout() == "progetti_esempio"): ?>
			<?php if( have_rows('inserisci_progetti') ): $po = 1 ?>
				<div class="row p-bottom">
					<div class="col-md-3 col-md-push-3 no-p hidden"><h4 class="black text-xs upp nx-b">Alcuni nostri progetti</h4>
					</div>
				</div>
				<div class="row">
					<?php while( have_rows('inserisci_progetti') ): the_row(); ?>

						<?php $post_object = get_sub_field('progetto'); if( $post_object ): $post = $post_object; setup_postdata( $post );  $img_header = get_field('immagine_header'); ?>
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 full-bg item-progetto no-p" style="background-image:url(<?php echo $img_header['url']; ?>)">
							<?php echo $po; ?>	
							<div class="img-overlay whiter"></div>

							<div class="cubotti">
								<a href="<?php the_permalink(); ?>" class="inner">
									<h4 class="black text-xs upp nx-b">
										<?php $terms = get_the_terms( $post->ID, 'cliente' );
										if ( !empty( $terms ) ){
											foreach ($terms as $term) {
												echo $term->name;
											}
										} ?>
									</h4>
									<hr class="hr-short-red">
									<h3 class="red text-sm">
										<?php the_field('titolo_progetto'); ?>
									</h3>
									<div class="pos-left-bottom p-left-2 p-bottom-2">
										<h4 class="black text-xs upp nx-b">
											<?php 
											$terms = get_the_terms( $post->ID, 'tipologia' );
											$t = count($terms)-1;
											if ( !empty( $terms ) ){
												foreach ($terms as $c=>$term) {
													echo ''.$term->name.'';
													if($c < $t ) echo ', ';
												}
											}
											?>
										</h4>
									</div>
									<div class="pos-right-bottom p-right-2 p-bottom-2">
										<span class="link-btn"></span>
									</div>
								</a>
							</div>
						</div>
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>
					<?php $po++ ?>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>

		<!-- Evidenza -->

		<?php elseif(get_row_layout() == "frase_evidenza"):  $evidenza = get_sub_field('evidenza');// layout: Featured Posts ?>

			<div class="row">
				<div class="container no-p">
					<div class="col-md-2">
						<div class="p-top-2 p-bottom-2">
						</div>
					</div>
					<div class="col-md-10">
						<p><?php echo $evidenza; ?></p>
					</div>
				</div>
			</div>

		<?php endif; ?>

	<?php endwhile; ?>

	<div class="row">
		<div class="container no-p">
			<div class="col-md-2">
				<div class="p-top-2 p-bottom-2"></div>
			</div>
			<div class="col-md-10 p-bottom-2">
				<div class="red-top-3 p-top">
					<h4 class="black text-xs upp nx-b hidden-xs"><?php _e('Pubblicata il','promemoria'); ?> <?php the_time('j M, Y'); ?></h4>
					<h4 class="black text-xs upp nx-b visible-xs"><small><?php _e('Pubblicata il','promemoria'); ?> </small><br><?php the_time('j M, Y'); ?></h4>

					<div class="pull-right mn-top mn-top-mobile">

						<?php $tit = get_the_title(); $url = get_permalink(); if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { 
    ADDTOANY_SHARE_SAVE_KIT( array( 'linkname' => $tit , 'linkurl' => $url ) );
} ?>
					</div>
				</div>

			</div>
		</div>
	</div>

<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
