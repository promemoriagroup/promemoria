<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Promemoria
 */

?>

<!-- Newsletter -->


<div class="row taglio-immagine newsletter pos-r" style="background-image:url('<?php echo get_bloginfo('template_directory');?>/img/mole.jpg')">
	<div class="img-overlay redest"></div>
	<div class="container">
		<div class="col-md-6">
				<div class="p-tot">
			<h3 class="white text-md p-top-2"><?php _e('Iscriviti alla nostra newsletter:','promemoria'); ?></h3>
			</div>
		</div>
		<div class="col-md-6 no-p-mobile">
			<div class="p-tot white">
				
					<!-- <input class="text-input m-bottom" type='email'>
					<div class="col-md-1 no-p-left">
						<input type="checkbox" required>
					</div>
					<div class="col-md-11 no-p-left">
						<p>
							Iscrivendoti alla newsletter acconsenti al trattamento dei dati personali. Acquisite le
							informazioni fornite dal titolare del trattamento ai sensi dell'articolo 13 del D.Lgs.
							196/2003, l'interessato: presta il suo consenso al trattamento dei dati personali per
							i fini indicati nella suddetta informativa.	
						</p>
					</div>
				
				<hr class="hr-white">
				<input class="link-btn white text-xs nx-b upp" type="submit" value="Iscriviti" />  -->

				<?php echo do_shortcode('[mc4wp_form id="559"]'); ?>
			
			</div>
		</div>
	</div>
</div>

<footer id="colophon" class="site-footer bg-extradark p-top-3 p-bottom-2" role="contentinfo">
		<div class="container">
			<div class="col-md-8 col-xs-12">
				<div class="col-md-2 no-p">
					<div class="logo-footer">
						<img src="<?php echo get_bloginfo('template_directory');?>/img/logo_foo.png">
					</div>
				</div>
				<div class="col-md-6 col-xs-12 pull-right no-p-mobile">
					<div class="city-add p-bottom">
						<h4 class="nx-b upp"><?php _e('TORINO','promemoria'); ?></h4>
						<p>via G. Pomba, 1-10123 Torino</p>
					</div>
					<div class="city-add p-bottom">
						<h4 class="nx-b upp">TREVISO</h4>
						<p>Via Albona, 11-31100 Treviso</p>
					</div>
					<div class="city-add p-bottom">
						<h4 class="nx-b upp"><?php _e('ROMA','promemoria'); ?></h4>
						<p>Via Luigi Fincati, 13-00154 Roma</p>
					</div>
					<div class="city-add p-bottom">
						<h4 class="nx-b upp"><?php _e('PARIGI','promemoria'); ?></h4>
						<p>47, rue de Lyon-75012 Paris</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="social">
					<ul>
						<li><a target="_blank" class="white sc icon-facebook" href="https://www.facebook.com/pages/Promemoria/207593449254213"></a></li>
						<li><a target="_blank" class="white sc icon-twitter" href="https://twitter.com/promemoria_it"></a></li>
						<li><a target="_blank" class="white sc icon-linkedin" href="https://www.linkedin.com/company/promemoria"></a></li>
						<li><a target="_blank" class="white sc icon-youtube" href="https://www.youtube.com/channel/UCjG5RrR0MefwtWsVWYDsPTQ"></a></li>
						<li><a target="_blank" class="white sc icon-issuu" href="https://issuu.com/promemoriagroup"></a></li>
					</ul>
				</div>
			</div>
		</div><!-- .site-info -->
		<div class="container p-row-top">
			<div class="col-md-4 col-xs-12">
				<small>2016 © Promemoria Srl<br>All Rights Reserved - P.I. 10540810016</small>
		    </div>
			<div class="col-md-8 col-xs-12">
					<div class="footer-menu">
					 <?php  wp_nav_menu( array(
				                        'theme_location'  => 'footer',
				                        'container'       => false,
				                        //'menu_class'      => 'nav navbar-nav',//  'nav navbar-right'
				                        'walker'          => new Bootstrap_Nav_Menu(),
				                        'fallback_cb'     => null,
						                'items_wrap'      => '%3$s',// skip the containing <ul>
				                    )
				                ); ?>
				    </div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
