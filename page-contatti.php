<?php
/**
* Template Name: Contatti
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
			<!-- immagine intro -->
			<div class="header-fade">
				<?php $header_image = get_field('immagine_header'); ?>
					<div class="row <?php if (!empty($header_image)) {echo 'taglio-immagine';} else {echo "taglio-immagine-vuoto";} ;?>" style="background-image:url('<?php echo $header_image['url']; ?>')">
						
					</div>
					<div class="row">
						<div class="container margin-calc">
							<div class="col-md-10 col-md-push-2 bg-white p-top-2 p-bottom p-left">	
								<?php $tit_color = get_field('colore_titolo') ?>
									<h2 class="red text-md p-left" style="color:<?php echo $tit_color ?>"><?php the_title(); ?></h2> 
									<div class="p-top-half p-left">
										<?php $sottotitolo = get_field('sottotitolo'); ?>
										<?php if ($sottotitolo): ?>
											<?php $sub_color = get_field('colore_sottotitolo') ?>
											<h3 class="black text-sm sottotitolo" style="color:<?php echo $sub_color ?>"><?php echo $sottotitolo; ?></h3>
										<?php endif ?>
									</div>
							</div>
						</div>
					</div>
			</div>

			<!-- teaser e sottotitolo -->
			<?php $teaser = get_field('teaser'); if ($teaser): ?>
			<div class="row p-row-top p-bottom-3">
				<div class="container no-p">
					<div class="col-md-2">
						<div class="p-top-2 p-bottom-2">
						</div>
					</div>
					<div class="col-md-10">
						<div class="red-top-1 p-top-2 p-bottom-2">
							<h3 class="red text-sm">
								<?php echo $teaser; ?>
							</h3>
						</div>
					</div>
				</div>	
			</div>
			<?php endif ?>

			<!-- Row bianca con il testo al centro  -->
			<div class="row bg-white">
				<div class="container">
					
				</div>
			</div>

			<!-- SEDI -->

<?php if( have_rows('inserisci_sede') ):  ?>

    	
		<div class="row p-top-3">
			<div class="container white-left-3 white-right-3 white-top-3 no-p">	
				<?php while(have_rows('inserisci_sede') ): the_row();  ?>


							<div class="row p-bottom-3 news-item h-eq">
							<div class="col-md-2 h-com p-top">
								<div class="red-top-3 p-top p-bottom-2">
									<h4 class="black text-xs upp nx-b"><?php the_sub_field('citta'); ?></h4>
								</div>
							</div>
							<div class="col-md-5 h-com ">
								<div class="p-top p-left p-right p-bottom">
									<?php $note = get_sub_field('note'); if($note) : ?>
										<p class="black text-xs upp nx-b smaller p-bottom-half"><?php echo $note ?></p>
									<?php endif; ?>
									<h3 class="black text-xs">
										<?php the_sub_field('indirizzo') ?>
									</h3>
									<div class="p-top">
									<?php $mail = get_sub_field('mail') ?>
									<p><?php the_sub_field('telefono') ?> <span class="red">─</span> <a href="mailto:<?php echo $mail; ?>"><?php echo $mail; ?></a></p>
									</div>
								</div>
							</div>
							<div class="col-md-5 h-com pos-r">
								<?php $map = get_sub_field('link_mappa') ?>
								<a href="<?php echo $map ?>" target="_blank" class="black nx-b hv-centered upp guardamappa icon-location-alt"><?php _e('guarda la mappa','promemoria'); ?></a>
							</div>	
						</div> 
				<?php endwhile; ?>
			</div>
		</div>
		<div class="row p-top-3 p-bottom-2 row-special-m-bottom">
			<div class="container no-p">
			</div>
		</div>
 	<?php endif; ?>

<?php endwhile; // End of the loop. ?>




<?php get_footer(); ?>


