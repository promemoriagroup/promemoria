<?php
/**
* Template Name: archivio
*/

get_header(); ?>

<!-- immagine intro -->
<div class="header-fade">
	<?php $header_image = get_field('immagine_header'); ?>
	<div class="row <?php if (!empty($header_image)) {echo 'taglio-immagine';} else {echo "taglio-immagine-vuoto";} ;?>" style="background-image:url('<?php echo $header_image['url']; ?>')">

	</div>
	<div class="row">
		<div class="container margin-calc">
			<div class="col-md-10 col-md-push-2 bg-white p-top-2 p-bottom p-left">	
				<?php $tit_color = get_field('colore_titolo') ?>
				<h2 class="red text-md p-left" style="color:<?php echo $tit_color ?>"><?php the_title(); ?></h2> 
				<div class="p-top-half p-left">
					<?php $sottotitolo = get_field('sottotitolo'); ?>
					<?php if ($sottotitolo): ?>
						<?php $sub_color = get_field('colore_sottotitolo') ?>
						<h3 class="black text-sm sottotitolo" style="color:<?php echo $sub_color ?>"><?php echo $sottotitolo; ?></h3>
					<?php endif ?>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Loop News : evidenza -->

<?php $evidenza = new WP_Query( array(
	'post_type'      => 'post', 
	'posts_per_page'    => '-1',
	'meta_key'		=> 'metti_in_evidenza',
	'meta_value'	=> 'si',
	'orderby'        => 'menu_order'
	)); ?>
	<?php if ($evidenza->have_posts()) : ?>  
		<div class="p-row-top p-row-bottom">
			<div class="h-eq">
				<div class="container">
					<?php while($evidenza->have_posts()) : $evidenza->the_post(); $img_cubo = get_field('immagine_cubotto'); ?>
						<a href="<?php the_permalink(); ?>" class="inner">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 full-bg news-evidenza" style="background-image:url(<?php echo $img_cubo['url']; ?>)">
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 p-tot h-com bg-pink">

								<div class="news-meta p-top p-left">
									<h4 class="black text-xs upp nx-b">
										<?php the_time('M j, Y'); ?>
									</h4>
									<hr class="hr-short-red">
									<h3 class="red text-sm">
										<?php the_title(); ?>
									</h3>
									<p class="p-top-2"><?php the_field('teaser_evidenza') ?></p>
								</div>
								<div class="pos-left-bottom p-left-2 p-bottom-3">
									<h4 class="black text-xs upp nx-b">
										IN EVIDENZA
									</h4>
								</div> 
								<div class="pos-right-bottom p-right-2 p-bottom-2">
									<span class="link-btn nx-b"><?php _e('Scopri','promemoria'); ?></span>
								</div>
							</div>
						</a> 
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php wp_reset_query(); ?>



	<!-- Loop News : tutte -->

	<?php $news = new WP_Query( array(
	'post_type'      => 'post', 
	'posts_per_page'    => '-1',
	'meta_query' => array(
        array(
            'key' => 'metti_in_evidenza',
            'value' => 'no'
        )
    ),
	'orderby'        => 'date'
	)); ?>
		<?php if ($news->have_posts()) : ?>  
			<div class="p-row-bottom">
				<div class="container">
					<?php while($news->have_posts()) : $news->the_post(); $short = get_field('is_short'); ?>
						<div class="row p-bottom-3">
							<div class="col-md-2">
								<div class="red-top-3 p-top-2 p-bottom-2">
									<h4 class="black text-xs upp nx-b"><?php the_time('j M, Y'); ?></h4>
								</div>
							</div>
							<div class="col-md-10">
								<div class="red-top-1 p-top-2 p-bottom-2">
									<?php if ($short == 'si'): ?>
										<h3 class="black text-sm">
											<?php the_title(); ?>
										</h3>
										<div class="p-top"><?php the_field('testo_short') ?></div>
									<?php else : ?>
										<h3 class="black text-sm">
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</h3>
									<?php endif; ?>
								</div>
								<div class="read-more upp">
									<?php if ($short == 'si'): ?>
										<span></span>
									<?php else : ?>
										<a class="link-btn" href="<?php the_permalink(); ?>"><?php _e('Scopri','promemoria'); ?></a>
									<?php endif; ?>
								</div>
							</div>
						</div> 
					<?php endwhile; ?>
				</div>
			</div>
		<?php endif; ?>
		<?php wp_reset_query(); ?>

		<!-- Loop News : in pillole -->

		<?php get_footer(); ?>


